﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pismeni
{
    class KazneniZapisnik : Zapisnik
    {
        private static string vrstaZapisnika;
        private static double maxKazna;
       
        private int brojKazni = 0;
        public KazneniZapisnik() : base(vrstaZapisnika, maxKazna)
        {
            VrstaZapisnika = vrstaZapisnika;
            MaxKazna = maxKazna;
            
        }

        public override void DodajKaznu(string ime, string prezime)
        {

            if (brojKazni < 1000)
            {
                brojKazni++;
            }
            else
            {
                throw (new PreviseKazni("Previse kriminala"));
            }
        }
    }
}
