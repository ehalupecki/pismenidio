﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pismeni
{
    sealed class Kazna
    {
        public Kazna(string ime, string prezime, int redniBroj, string opis)
        {
            Ime = ime;
            Prezime = prezime;
            RedniBroj = redniBroj;
            Datum = DateTime.Now;
            Opis = opis;
        }
        public override string ToString()
        {
            return RedniBroj + " " + Ime + " " + Prezime + " " + Datum + " - " + Opis;
        }
        public string Ime { get; set; }

        public string Prezime { get; set; }

        public int RedniBroj { get; set; }

        public DateTime Datum { get; set; }

        public string Opis { get; set; }
    }
}
