﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pismeni
{
    interface IZapisnik
    {

        public abstract void DodajKaznu(string ime, string prezime);
        public virtual void PrikaziKazne() { }
    }
    abstract class Zapisnik : IZapisnik
    {
        public Zapisnik(string vrstaZapisnika, double maxKazna)
        {
            VrstaZapisnika = vrstaZapisnika;
            MaxKazna = maxKazna;
        }

        public abstract void DodajKaznu(string ime, string prezime);

        public virtual void PrikaziKazne()
        {
            Console.WriteLine(Kazne.GetEnumerator());
        }

        public string VrstaZapisnika { get; set; }
        public double MaxKazna
        {
            get
            {
                return MaxKazna;
            }
            set
            {
                MaxKazna = value;
            }
        }
        protected IEnumerable<Kazna> Kazne;

    }
}

