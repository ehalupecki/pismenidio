﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pismeni
{
    public class PreviseKazni : Exception
    {
        public PreviseKazni(string message) : base(message)
        {
        }
    }
    class PrometniZapisnik : Zapisnik
    {
        private static string vrstaZapisnika;
        private static double maxKazna;
        private int brojKazni = 151;
        public PrometniZapisnik() : base(vrstaZapisnika, maxKazna)
        {
            vrstaZapisnika = "Prometni zapisnik";
            
        }
        public override void DodajKaznu(string ime, string prezime)
        {
            if (brojKazni < 500)
            {
                brojKazni++;
            }
            else
            {
                throw (new PreviseKazni("Proracun Pun"));
            }
            
        }
    }
}
